<div dir="rtl" align="right" markdown="1" style="font-family: 'Vazirmatn RD', IRANSansX, Arial, sans-serif;">

# راهنمای اس‌کیوال و پستگرس‌کیوال

## اجرایدن سمپاد

پس از نصبیدن [داکر](https://docs.docker.com/engine/install/) و [داکرکامپوز](https://docs.docker.com/engine/install/)، برای اجرایدن پستگرس‌کیوال دستور زیر را در خط فرمان در ریشه‌ی فایل `docker-compose.yml` باجرای:

</div>

```shell
docker-compose up
```

<div dir="rtl" align="right" markdown="1" style="font-family: 'Vazirmatn RD', IRANSansX, Arial, sans-serif;">

سپس از اطلاعات زیر برای وصلیدن به سمپادت، در مسیر http://localhost:3040، باستفای:

</div>

* pgAdmin:
  * Email: admin@email.com
  * Password: password
* PostgreSQL:
  * Host name: pgdb
  * Database: my_db
  * User: root
  * Password: password
